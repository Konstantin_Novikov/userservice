// Package postgres contains models.Store's realization on postgresql.
package postgres

import (
	"context"
	"database/sql"
	"fmt"
	"log"

	_ "github.com/lib/pq"
	"gitlab.com/Konstantin_Novikov/userservice/pkg/models"
)

// Store is model of interaction with postgres database
type Store struct {
	db *sql.DB
}

// NewStore builds store object
func NewStore(addr, port, user, password string) (*Store, error) {
	var (
		store Store
		err   error
	)

	// Connect with database
	query := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
		addr, port, user, password, user)
	store.db, err = sql.Open("postgres", query)
	if err != nil {
		return nil, fmt.Errorf("NewStore: %w", err)
	}

	if err = store.db.Ping(); err != nil {
		return nil, fmt.Errorf("NewStore: %w", err)
	}

	return &store, nil
}

// CreateUser is for writing user's data into a database
func (s *Store) CreateUser(ctx context.Context, user models.User, passwordHash string) (*models.User, error) {
	roleID, err := s.getRoleID(ctx, models.DbRoleUser)
	if err != nil {
		return nil, err
	}

	stmt, err := s.db.Prepare(`INSERT INTO "user" (first_name, last_name, password, date_registration, role_id) ` +
		`VALUES ($1, $2, $3, $4, $5);`)
	if err != nil {
		log.Printf("[ERROR] Store.CreateUser: %v", err)
		return nil, fmt.Errorf("Store.CreateUser: %w", models.ErrDatastoreIssue)
	}
	defer stmt.Close()

	if _, err = stmt.Exec(user.FirstName, user.LastName, passwordHash, user.DateRegistration, roleID); err != nil {
		log.Printf("[ERROR] Store.CreateUser: %v", err)
		return nil, fmt.Errorf("Store.CreateUser: %w", models.ErrDatastoreIssue)
	}

	return &user, nil
}

// DeleteUser fills nulls and default values into user's record in database.
// If we want to save user's data and only makes him not-active
// we should use UpdateUserRole instead
func (s *Store) DeleteUser(ctx context.Context, uid int64) error {
	log.Printf("[WARNING]: Store.DeleteUser called, but it's a dummy func for now")

	return nil
}

// UpdateUserRole changes user's role in database
func (s *Store) UpdateUserRole(ctx context.Context, uid int64, roleName string) error {
	roleID, err := s.getRoleID(ctx, roleName)
	if err != nil {
		return err
	}

	stmt, err := s.db.Prepare(`UPDATE "user" SET "role_id" = $1 WHERE "id" = $2;`)
	if err != nil {
		log.Printf("[ERROR] Store.UpdateUserRole: %v", err)
		return fmt.Errorf("Store.UpdateUserRole: %w", models.ErrDatastoreIssue)
	}
	defer stmt.Close()

	result, err := stmt.Exec(roleID, uid)
	if err != nil {
		log.Printf("[ERROR] Store.UpdateUserRole: %v", err)
		return fmt.Errorf("Store.UpdateUserRole: %w", models.ErrDatastoreIssue)
	}

	// Check if we don't found any user with uid
	rowsAffected, _ := result.RowsAffected()
	if rowsAffected == 0 {
		log.Printf("[ERROR] Store.UpdateUserRole: user with UID = %d is not found", uid)
		return fmt.Errorf("Store.UpdateUserRole: %w", models.ErrUserNotFound)
	}

	return nil
}

// GetUserByID returns user's private and public data by user's ID
func (s *Store) GetUserByID(ctx context.Context, uid int64) (*models.User, error) {
	stmt, err := s.db.Prepare(`SELECT "user".id, "user".first_name, "user".last_name, "user".date_registration, "role".name FROM "user"
		INNER JOIN "role"
		ON "user".role_id = "role".id 
		WHERE "user".id = $1;`)
	if err != nil {
		log.Printf("[ERROR] Store.GetUserByID: %v", err)
		return nil, fmt.Errorf("Store.GetUserByID: %w", models.ErrDatastoreIssue)
	}
	defer stmt.Close()

	var user models.User
	if err = stmt.QueryRow(uid).Scan(&user.ID,
		&user.FirstName,
		&user.LastName,
		&user.DateRegistration,
		&user.Role); err != nil {
		log.Printf("[ERROR] Store.GetUserByID: %v", err)
		if err == sql.ErrNoRows {
			return nil, fmt.Errorf("Store.GetUserByID: %w", models.ErrUserNotFound)
		}

		return nil, fmt.Errorf("Store.GetUserByID: %w", models.ErrDatastoreIssue)
	}

	return &user, nil
}

// GetUsers returns one page of records with user's private and public data.
// Parameter 'count' sets how many user's records will be returned (we can name it page's size).
// Parameter 'offset' is a page's number.
func (s *Store) GetUsers(ctx context.Context, offset int64, count int64) ([]models.User, error) {
	// Get data from DB
	stmt, err := s.db.Prepare(`SELECT "user".id, "user".first_name, "user".last_name, "user".date_registration, "role".name FROM "user"
		INNER JOIN "role"
		ON "user".role_id = "role".id
		ORDER BY "user"."id"
		LIMIT $1 
		OFFSET $2
	;`)
	if err != nil {
		log.Printf("[ERROR] Store.GetUsers: %v", err)
		return nil, fmt.Errorf("Store.GetUsers: %w", models.ErrDatastoreIssue)
	}
	defer stmt.Close()

	rows, err := stmt.Query(count, offset*count)
	if err != nil {
		log.Printf("[ERROR] Store.GetUsers: %v", err)
		return nil, fmt.Errorf("Store.GetUsers: %w", models.ErrDatastoreIssue)
	}
	defer rows.Close()

	// Parse data into struct
	users := make([]models.User, 0, count)
	for rows.Next() {
		var user models.User
		rows.Scan(&user.ID,
			&user.FirstName,
			&user.LastName,
			&user.DateRegistration,
			&user.Role)
		users = append(users, user)
	}
	if len(users) == 0 {
		log.Printf("Store.GetUsers: %w", models.ErrPageIsEmpty)
		return nil, fmt.Errorf("Store.GetUsers: %w", models.ErrPageIsEmpty)
	}

	return users, nil
}

// getRoleID returns user-role's id by its name from database
func (s *Store) getRoleID(ctx context.Context, name string) (int64, error) {
	rows, err := s.db.Query(`SELECT "id" FROM "role" WHERE "name" = $1;`, name)
	if err != nil {
		log.Printf("Store.getRoleID: %w", models.ErrDatastoreIssue)
		return 0, err
	}
	defer rows.Close()

	// Extract role's id
	var id int64
	rows.Next()
	if err = rows.Scan(&id); err != nil {
		return 0, fmt.Errorf("Store.getRoleID: %w", models.ErrDatastoreIssue)
	}

	return id, nil
}
