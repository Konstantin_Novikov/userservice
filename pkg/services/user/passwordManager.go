package user

import (
	"fmt"
	"log"

	"gitlab.com/Konstantin_Novikov/userservice/pkg/models"

	"golang.org/x/crypto/bcrypt"
)

// passwordManager makes manipulations with passwords
type passwordManager struct{}

// isPasswordValid checks password by list of rules.
// If password isn't valid, its describes problem in error
func (pm passwordManager) isPasswordValid(password string) (bool, error) {
	const (
		pwdLen = 8
	)

	// Check password's length
	if len(password) < pwdLen {
		log.Printf("[ERROR] passwordManager.isPasswordValid: %v (len < %d)", models.ErrPasswordTooWeak, pwdLen)
		return false, fmt.Errorf("passwordManager.isPasswordValid: %w (len < %d)", models.ErrPasswordTooWeak, pwdLen)
	}

	return true, nil
}

// calcHashPassword calculates hash from password
func (pm passwordManager) calcHashPassword(password string) (string, error) {
	hash, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", fmt.Errorf("passwordManager.calcHashPassword: %w", models.ErrPasswordsNotEqual)
	}

	return string(hash), nil
}

// compareHashAndPassword checks, if 'password' and password from 'hashedPassword' are equal
func (pm passwordManager) compareHashAndPassword(password string, hashedPassword string) (bool, error) {
	if err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password)); err != nil {
		return false, fmt.Errorf("passwordManager.compareHashAndPassword: %w", models.ErrPasswordsNotEqual)
	}

	return true, nil
}
