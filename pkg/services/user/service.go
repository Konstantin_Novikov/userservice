// Package user contains users management service,
// which controls abstract store
package user

import (
	"context"
	"log"
	"time"

	"gitlab.com/Konstantin_Novikov/userservice/pkg/models"
)

// UserService is implementation of models.userService
type UserService struct {
	store  models.Store
	pwdMan passwordManager
}

// NewUserService creates new copy of UserService
func NewUserService(store models.Store) (*UserService, error) {
	return &UserService{
		store:  store,
		pwdMan: passwordManager{},
	}, nil
}

// CreateUser do creating new user and saving him in store
func (s *UserService) CreateUser(ctx context.Context, user models.User, password string) (*models.User, error) {
	valid, err := s.pwdMan.isPasswordValid(password)
	if !valid {
		return nil, err
	}

	pwdHash, err := s.pwdMan.calcHashPassword(password)
	if err != nil {
		return nil, err
	}

	user.DateRegistration = time.Now()

	// Use store to create user and write data into database
	u, err := s.store.CreateUser(ctx, user, pwdHash)
	if err != nil {
		return nil, err
	}
	log.Printf("[DEBUG] UserService.CreateUser: created user")

	return u, nil
}

// DeleteUser sets user's status in 'not-active' state in store
func (s *UserService) DeleteUser(ctx context.Context, id int64) error {
	if err := s.store.UpdateUserRole(ctx, id, models.DbRoleDeleted); err != nil {
		return err
	}

	log.Printf("[DEBUG] UserService.DeleteUser: deleted user")
	return nil
}

// GetUserByID returns only public user's data
func (s *UserService) GetUserByID(ctx context.Context, uid int64) (*models.User, error) {
	user, err := s.store.GetUserByID(ctx, uid)
	if err != nil {
		return nil, err
	}

	return user, nil
}

// GetUsers returns multiple users's data
func (s *UserService) GetUsers(ctx context.Context, offset int64, count int64) ([]models.User, error) {
	users, err := s.store.GetUsers(ctx, offset, count)
	if err != nil {
		return nil, err
	}

	return users, nil
}
