// Package models contains interfaces and structures,
// which may be used to build application for users management
// in abstract store.
package models

import (
	"context"
	"time"
)

// UserService contains a list of methods which must be implemented by user's models in other packages
type UserService interface {
	CreateUser(ctx context.Context, user User, password string) (*User, error)
	DeleteUser(ctx context.Context, id int64) error

	GetUserByID(ctx context.Context, id int64) (*User, error)
	GetUsers(ctx context.Context, offset int64, count int64) ([]User, error)
}

// User is structure with all public fields
type User struct {
	ID               int64     `json:"id"`
	FirstName        string    `json:"firstName"`
	LastName         string    `json:"lastName"`
	Role             string    `json:"role"`
	DateRegistration time.Time `json:"dateRegistration"`
}
