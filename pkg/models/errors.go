package models

import "errors"

var (
	ErrInternalError     = errors.New("Internal server error")
	ErrInvalidParameter  = errors.New("Invalid parameter received")
	ErrDatastoreIssue    = errors.New("Datastore problem")
	ErrPasswordTooWeak   = errors.New("User's password is too weak")
	ErrPasswordsNotEqual = errors.New("Passwords are not equal")
	ErrPageIsEmpty       = errors.New("Pagination problem (requested page is empty, 0 records returned)")
	ErrUserNotFound      = errors.New("User is not found")
)
