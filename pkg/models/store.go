package models

import "context"

// Store contains a list of methods which should be implemented by database's models
type Store interface {
	CreateUser(ctx context.Context, user User, password string) (*User, error)
	DeleteUser(ctx context.Context, id int64) error
	UpdateUserRole(ctx context.Context, uid int64, roleName string) error

	GetUserByID(ctx context.Context, id int64) (*User, error)
	GetUsers(ctx context.Context, offset int64, count int64) ([]User, error)
}

// User's roles
const (
	DbRoleAdmin   = "admin"
	DbRoleUser    = "user"
	DbRoleDeleted = "deleted"
)
