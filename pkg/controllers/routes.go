// Package controllers contains http handlers of application's API
package controllers

import (
	"context"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"

	"gitlab.com/Konstantin_Novikov/userservice/pkg/models"
)

// Routes create gorilla/mux router, sets http-routes and returns it
func Routes(userService models.UserService) http.Handler {
	router := mux.NewRouter()

	router.HandleFunc("/user/{uid:[0-9]+}", handleUserGetByID(userService)).Methods(http.MethodGet)
	router.HandleFunc("/user/{uid:[0-9]+}", handleUserDelete(userService)).Methods(http.MethodDelete)
	router.HandleFunc("/user", handleUserGet(userService)).Methods(http.MethodGet)
	router.HandleFunc("/user", handleUserCreate(userService)).Methods(http.MethodPost)

	return router
}

// Pagination constants
const (
	DefaultPageNumber = 0
	DefaultPageSize   = 20
)

// MessageUserCreate contains information, which will be used to create new user by UserService
type MessageUserCreate struct {
	FirstName string `json:"firstName"`
	LastName  string `json:"lastName"`
	Password  string `json:"password"`
}

// handleUserCreate wait for MessageUserCreate in http.Body
func handleUserCreate(userService models.UserService) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("[DEBUG] handleUserCreate called")

		ctx := context.TODO()
		body, err := ioutil.ReadAll(r.Body)
		if err != nil {
			log.Printf("[ERROR] handleUserCreate: %v", err)
			sendJSONError(w, models.ErrInternalError, "Cannot parse HTTP-body")
			return
		}

		// Extract user's data from request
		var msg MessageUserCreate
		if err := json.Unmarshal(body, &msg); err != nil {
			log.Printf("[ERROR] handleUserCreate: %v", err)
			sendJSONError(w, models.ErrInvalidParameter, "Invalid JSON with user description received")
			return
		}

		// Create user
		user := models.User{
			FirstName: msg.FirstName,
			LastName:  msg.LastName,
		}

		if _, err = userService.CreateUser(ctx, user, msg.Password); err != nil {
			sendJSONError(w, err, "")
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}

func handleUserDelete(userService models.UserService) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("[DEBUG] handleUserDelete called")

		// Parse user's id from http.Request
		vars := mux.Vars(r)
		uidStr, ok := vars["uid"]
		if !ok {
			log.Printf("[ERROR] handleUserDelete: error when parsing user's ID")
			sendJSONError(w, models.ErrInvalidParameter, "UID isn't correct")
			return
		}

		uid, err := strconv.ParseInt(uidStr, 10, 64)
		if err != nil {
			log.Printf("[ERROR] handleUserDelete: error when converting user's ID")
			sendJSONError(w, models.ErrInvalidParameter, "UID isn't correct")
			return
		}

		// Send request to user service
		ctx := context.TODO()
		if err = userService.DeleteUser(ctx, uid); err != nil {
			sendJSONError(w, err, "")
			return
		}

		w.WriteHeader(http.StatusOK)
	}
}

func handleUserGetByID(userService models.UserService) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("[DEBUG] handleUserGetByID called")

		// Parse user's id from http.Request
		vars := mux.Vars(r)
		uidStr, ok := vars["uid"]
		if !ok {
			log.Printf("[ERROR] handleUserGetByID: error when parsing user's ID")
			w.WriteHeader(http.StatusBadRequest)
			return
		}

		uid, err := strconv.ParseInt(uidStr, 10, 64)
		if err != nil {
			log.Printf("[ERROR] handleUserGetByID: error when converting user's ID")
			w.WriteHeader(http.StatusBadRequest)
		}

		// Send request to user service
		ctx := context.TODO()
		user, err := userService.GetUserByID(ctx, uid)
		if err != nil {
			sendJSONError(w, err, "")
			return
		}

		// Build answer
		data, err := json.Marshal(user)
		if err != nil {
			log.Printf("[ERROR] handleUserGetByID: %v", err)
			sendJSONError(w, models.ErrInternalError, "")
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(data)
	}
}

func handleUserGet(userService models.UserService) func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		log.Println("[DEBUG] handleUserGet is called")

		// Function will parse int64 with default value, if source string is empty
		int64orDefault := func(data string, defaultValue int64) (int64, error) {
			if data == "" {
				return defaultValue, nil
			}
			value, err := strconv.ParseInt(data, 10, 64)
			if err != nil {
				log.Printf("[ERROR] parseInt64WithDefault: %v")
				return 0, models.ErrInvalidParameter
			}

			return value, nil
		}

		// Retrieving query's parameters from URL query
		values := r.URL.Query()
		pageNumber, err := int64orDefault(values.Get("page_number"), DefaultPageNumber)
		if err != nil {
			sendJSONError(w, err, "Wrong 'page_number' parameter's value")
			return
		}
		if pageNumber < 0 {
			log.Printf("[ERROR] handleUserGet: 'page_offset' should be greater or equal to 0 (received %v)", pageNumber)
			sendJSONError(w, models.ErrInvalidParameter, "'page_offset' should be greater or equal to 0")
			return
		}

		pageSize, err := int64orDefault(values.Get("page_size"), DefaultPageSize)
		if err != nil {
			sendJSONError(w, err, "Wrong 'page_size' parameter's value")
			return
		}
		if pageSize <= 0 {
			log.Printf("[ERROR] handleUserGet: 'page_size' should be greater than 0 (received %v)", pageSize)
			sendJSONError(w, models.ErrInvalidParameter, "'page_size' should be greater than 0")
			return
		}

		// Send request to UserService
		ctx := context.TODO()
		user, err := userService.GetUsers(ctx, pageNumber, pageSize)
		if err != nil {
			sendJSONError(w, err, "")
			return
		}

		// Build answer
		data, err := json.Marshal(user)
		if err != nil {
			log.Printf("[ERROR] handleUserGet: error when marshalling user into JSON (%v)", err)
			sendJSONError(w, models.ErrInternalError, "")
			return
		}

		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusOK)
		w.Write(data)
	}
}
