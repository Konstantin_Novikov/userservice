package controllers

import (
	"encoding/json"
	"errors"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/Konstantin_Novikov/userservice/pkg/models"
)

type MessageResponseError struct {
	StatusCode  int    `json:"statusCode"`
	Error       string `json:"error"`
	Description string `json:"description"`
}

// getErrHTTPStatusCode returns HTTP-status codes for service's errors,
// which declared in models packages
func getErrHTTPStatusCode(err error) (int, error) {
	switch err {
	case models.ErrDatastoreIssue:
		return http.StatusInternalServerError, nil
	case models.ErrPageIsEmpty:
		return http.StatusNotFound, nil
	case models.ErrPasswordsNotEqual:
		return http.StatusBadRequest, nil
	case models.ErrPasswordTooWeak:
		return http.StatusBadRequest, nil
	case models.ErrUserNotFound:
		return http.StatusNotFound, nil
	case models.ErrInvalidParameter:
		return http.StatusBadRequest, nil
	}

	return 0, fmt.Errorf("getErrHTTPStatusCode: error isn't found in list")
}

// sendError build error in JSON and sends it via http.Request
func sendJSONError(w http.ResponseWriter, wrappedErr error, desc string) error {
	// Try to unwrap input error
	e := errors.Unwrap(wrappedErr)
	if e == nil {
		e = wrappedErr // Maybe we got unwrapped models.Err
	}

	statusCode, err := getErrHTTPStatusCode(e)
	if err != nil {
		return err
	}
	w.WriteHeader(statusCode)

	// Build struct, which will be send as answer
	msgError := &MessageResponseError{
		StatusCode:  statusCode,
		Error:       e.Error(),
		Description: desc,
	}
	data, err := json.Marshal(&msgError)
	if err != nil {
		log.Printf("[ERROR] sendJSONError: cannot build messageError (%v)", err)
		return fmt.Errorf("sendJSONError: cannot build messageError (%w)", err)
	}

	w.Write(data)

	return nil
}
