FROM golang:1.14-rc-alpine

WORKDIR /usr/src/userService
COPY . .

WORKDIR /usr/src/userService/cmd/userService
RUN go build

CMD [ "./userService" ]
