# UserService

UserService is a small user management service, which holds information about user's accounts. You can simply create new user, delete him or get information via API.

UserService has been written in Golang. Postgres used as DB, Docker used to pack services into images, Docker-compose used to launch and link all services.

# API

This is list of available http-requests.

Service supports pagination. You can add this parameters in your query:
- page_number (default: 0)
- page_size (default: 20)

| Name | Path | Query parameters | HTTP-body
| ------ | ------ | ------ | ------ |
| Get user by ID | GET /user/[id] | no additional parameters | empty |
| Get list of users | GET /user | - page\_number: number of page <br>- page\_size: how many records you want to get by your request | empty |
| Create user | POST /user | no additional parameters | User in JSON (example):<br>{<br><dd>"firstName": "FirstName",<br>"lastName": "LastName",<br>"password": "some-password"</dd>}|
| Delete user | DELETE /user/[id] | no additional parameters | empty |