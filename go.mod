module gitlab.com/Konstantin_Novikov/userservice

go 1.13

require (
	github.com/gorilla/mux v1.7.3
	github.com/jessevdk/go-flags v1.4.0
	github.com/lib/pq v1.3.0
	golang.org/x/crypto v0.0.0-20200117160349-530e935923ad
)
