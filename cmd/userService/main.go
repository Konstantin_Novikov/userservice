package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/jessevdk/go-flags"
	"gitlab.com/Konstantin_Novikov/userservice/pkg/controllers"
	"gitlab.com/Konstantin_Novikov/userservice/pkg/services/user"
	"gitlab.com/Konstantin_Novikov/userservice/pkg/store/postgres"
)

// opts contains all cli parameters, that program needs
type cliOpts struct {
	ServerAddr string `long:"serverAddr" env:"SERVER_ADDR" required:"true" description:"Application server's IP addr"`
	ServerPort string `long:"serverPort" env:"SERVER_PORT" required:"true" description:"Application server's port"`

	DatabaseAddr     string `long:"databaseAddr" env:"DATABASE_ADDR"required:"true" description:"Database's IP addr"`
	DatabasePort     string `long:"databasePort" env:"DATABASE_PORT" required:"true" description:"Database's port"`
	DatabaseUser     string `long:"databaseUser" env:"DATABASE_USER" required:"true" description:"username in db"`
	DatabasePassword string `long:"databasePassword" env:"DATABASE_PASSWORD" required:"true" description:"password in db"`
}

func main() {
	var opts cliOpts
	if _, err := flags.Parse(&opts); err != nil {
		log.Fatalf("[ERROR] %v", err)
	}

	// Create Store and User service
	store, err := postgres.NewStore(opts.DatabaseAddr, opts.DatabasePort, opts.DatabaseUser, opts.DatabasePassword)
	if err != nil {
		log.Fatalf("[ERROR] %v", err)
	}

	userService, err := user.NewUserService(store)
	if err != nil {
		log.Fatalf("[ERROR] %v", err)
	}

	// Run http server
	httpServer := http.Server{
		Addr:    fmt.Sprintf("%s:%s", opts.ServerAddr, opts.ServerPort),
		Handler: controllers.Routes(userService),
	}

	log.Printf("[DEBUG] main(): ListenAndServe on %s", httpServer.Addr)
	if err := httpServer.ListenAndServe(); err != nil {
		log.Printf("[ERROR] ListenAndServe(): closed with error (%v)", err)
	}
}
